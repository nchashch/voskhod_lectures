#!/usr/bin/emacs --script
(require 'ox-publish)
(setq org-publish-project-alist
  '(
    ("lecture-notes"
    :base-directory "./src/"
    :base-extension "org"
    :publishing-directory "./exported_pdf/"
    :recursive t
    :publishing-function org-latex-publish-to-pdf
    )
  ))
(org-publish "lecture-notes" t)
